////////////////////////////////////////////////////////////
// CANVAS LOADER
////////////////////////////////////////////////////////////

 /*!
 * 
 * START CANVAS PRELOADER - This is the function that runs to preload canvas asserts
 * 
 */
function initPreload(){
	toggleLoader(true);
	checkMobileEvent();
	
	$(window).resize(function(){
		resizeGameFunc();
	});
	resizeGameFunc();
	
	loader = new createjs.LoadQueue(false);
	manifest = [{src:'assets/bgMain.jpg', id:'bgLanding'},
				{src:'assets/bgGame.jpg', id:'bgGame'},
				{src:'assets/btnStart.png', id:'btnStart'},
				{src:'assets/bgOverlay.png', id:'bgPop'},
				{src:'assets/ball_shadow.png', id:'gameBallShadow'},
				{src:'assets/ball.png', id:'gameBall'},
				{src:'assets/player_Spritesheet7x1.png', id:'gamePlayer'},
				{src:'assets/bar_blank.png', id:'powerBarBlank'},
				{src:'assets/bar_colour.png', id:'powerBar'},
				{src:'assets/bar.png', id:'powerBarBg'},
				
				{src:'assets/btnReplay.png', id:'btnReplay'},
				{src:'assets/btnHome.png', id:'btnBackMain'},
				{src:'assets/btnFacebook.png', id:'btnFb'},
				{src:'assets/btnTwitter.png', id:'btnTwitter'},
				{src:'assets/btnGoogle.png', id:'btnGoogle'}
				];
	
	for(n=0;n<binobject_arr.length;n++){
		manifest.push({src:binobject_arr[n].src, id:'bin'+n})
	}
	
	soundOn = true;		
	if($.browser.mobile || isTablet){
		if(!enableMobileSound){
			soundOn=false;
		}
	}
	
	if(soundOn){
		manifest.push({src:'assets/sounds/ball_hit.ogg', id:'soundBallHit'});
		manifest.push({src:'assets/sounds/click.ogg', id:'soundClick'});
		manifest.push({src:'assets/sounds/fail.ogg', id:'soundFail'});
		manifest.push({src:'assets/sounds/score.ogg', id:'soundScore'});
		manifest.push({src:'assets/sounds/lose.ogg', id:'soundLose'});
		
		createjs.Sound.alternateExtensions = ["mp3"];
		loader.installPlugin(createjs.Sound);
	}
	
	loader.addEventListener("complete", handleComplete);
	loader.addEventListener("fileload", fileComplete);
	loader.addEventListener("error",handleFileError);
	loader.on("progress", handleProgress, this);
	loader.loadManifest(manifest);
}

/*!
 * 
 * CANVAS FILE COMPLETE EVENT - This is the function that runs to update when file loaded complete
 * 
 */
function fileComplete(evt) {
	var item = evt.item;
	console.log("Event Callback file loaded ", evt.item.id);
}

/*!
 * 
 * CANVAS FILE HANDLE EVENT - This is the function that runs to handle file error
 * 
 */
function handleFileError(evt) {
	console.log("error ", evt);
}

/*!
 * 
 * CANVAS PRELOADER UPDATE - This is the function that runs to update preloder progress
 * 
 */
function handleProgress() {
	$('#mainLoader').html(Math.round(loader.progress/1*100));
}

/*!
 * 
 * CANVAS PRELOADER COMPLETE - This is the function that runs when preloader is complete
 * 
 */
function handleComplete() {
	toggleLoader(false);
	initMain();
};

/*!
 * 
 * TOGGLE LOADER - This is the function that runs to display/hide loader
 * 
 */
function toggleLoader(con){
	if(con){
		$('#mainLoader').show();
	}else{
		$('#mainLoader').hide();
	}
}