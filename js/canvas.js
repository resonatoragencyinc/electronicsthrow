////////////////////////////////////////////////////////////
// CANVAS
////////////////////////////////////////////////////////////
var stage
var canvasW=0;
var canvasH=0;

/*!
 * 
 * START GAME CANVAS - This is the function that runs to setup game canvas
 * 
 */
function initGameCanvas(w,h){
	canvasW=w;
	canvasH=h;
	stage = new createjs.Stage("gameCanvas");
	
	createjs.Touch.enable(stage);
	stage.enableMouseOver(20);
	stage.mouseMoveOutside = true;
	
	createjs.Ticker.setFPS(60);
	createjs.Ticker.addEventListener("tick", tick);	
}

var canvasContainer, mainContainer, gameContainer, resultPopContainer;
var bgLanding, bgGame, btnStart, bgPop, bgOverlay, gameBallShadow, gameBall, gamePlayer, playerdata, powerBarBlank, powerBar, powerBarBg, powerBarMaskBitmap, scoreDescTxt, scoreTxt, lifeDescTxt, lifeTxt, btnReplay, btnBackMain, btnFb, btnTwitter, btnGoogle, resultScoreDescTxt, resultScoreTxt, resultShareTxt, scoreDisplayTxt;

$.bin ={};
var txtExtra=0;

/*!
 * 
 * BUILD GAME CANVAS ASSERTS - This is the function that runs to build game canvas asserts
 * 
 */
function buildGameCanvas(){
	canvasContainer = new createjs.Container();
	mainContainer = new createjs.Container();
	gameContainer = new createjs.Container();
	resultPopContainer = new createjs.Container();
	
	//main
	bgLanding = new createjs.Bitmap(loader.getResult("bgLanding"));
	btnStart = new createjs.Bitmap(loader.getResult("btnStart"));
	mainContainer.addChild(bgLanding, btnStart);
	
	centerReg(btnStart);
	btnStart.x=canvasW/2;
	btnStart.y=canvasH/100*55;
	
	bgGame = new createjs.Bitmap(loader.getResult("bgGame"));
	
	var _frameW=180;
	var _frameH=270;
	var _frame = {"regX": (_frameW/2), "regY": (_frameH/2), "height": _frameH, "count": 7, "width": _frameW};
	var _animations = {static:{frames: [0,1], speed: .1},
					   hitpower:{frames: [4,6], speed: .5, next:"powerlast"},
					   powerlast:[6],
					   hitsoft:{frames: [2,3], speed: .5, next:"softlast"},
					   softlast:[3]};
	//game
	playerdata = new createjs.SpriteSheet({
		"images": [loader.getResult("gamePlayer").src],
		"frames": _frame,
		"animations": _animations
	});
	gamePlayer = new createjs.Sprite(playerdata, "static");
	gamePlayer.framerate = 20;
	gamePlayer.x=canvasW/100*15;
	gamePlayer.y=canvasH/100*58;
	
	gameBallShadow = new createjs.Bitmap(loader.getResult("gameBallShadow"));
	gameBall = new createjs.Bitmap(loader.getResult("gameBall"));
	
	centerReg(gameBallShadow);
	centerReg(gameBall);
	
	if(!$.browser.mozilla){
		txtExtra=-3;
	}
	
	powerBarBlank = new createjs.Bitmap(loader.getResult("powerBarBlank"));
	powerBar = new createjs.Bitmap(loader.getResult("powerBar"));
	powerBarBg = new createjs.Bitmap(loader.getResult("powerBarBg"));
	
	centerReg(powerBarBlank);
	centerReg(powerBar);
	centerReg(powerBarBg);
	
	powerBarBlank.x=powerBar.x=powerBarBg.x=canvasW/2;
	powerBarBlank.y=powerBar.y=powerBarBg.y=canvasH/100*88;
	
	powerBarMaskBitmap = new createjs.Shape();
	powerBar.mask = powerBarMaskBitmap;
	
	for(n=0;n<binobject_arr.length;n++){
		$.bin['bin'+n] = new createjs.Bitmap(loader.getResult('bin'+n));
		$.bin['bin'+n].regX=$.bin['bin'+n].image.naturalWidth/2;
		$.bin['bin'+n].regY=$.bin['bin'+n].image.naturalHeight;
		$.bin['bin'+n].y=-100;
		gameContainer.addChild($.bin['bin'+n]);
	}
	
	scoreDescTxt = new createjs.Text();
	scoreTxt = new createjs.Text();
	scoreDescTxt.font = "60px comfortaabold";
	scoreTxt.font = "100px comfortaabold";
	scoreDescTxt.color = "#ffffff";
	scoreTxt.color = "#ffcc00";
	scoreDescTxt.text = scoreDisplayText;
	scoreTxt.text = "0";
	scoreDescTxt.textAlign = scoreTxt.textAlign = "left";
	
	lifeDescTxt = new createjs.Text();
	lifeTxt = new createjs.Text();
	lifeDescTxt.font = "60px comfortaabold";
	lifeTxt.font = "100px comfortaabold";
	lifeDescTxt.color = "#ffffff";
	lifeTxt.color = "#cb2931";
	lifeDescTxt.text = lifeDisplayText;
	lifeTxt.text = "0";
	lifeDescTxt.textAlign = lifeTxt.textAlign = "right";
	
	scoreDescTxt.x=canvasW/100 * 4;
	scoreDescTxt.y=(canvasH/100 * 4)+txtExtra;
	scoreTxt.x=canvasW/100 * 25;
	scoreTxt.y=(canvasH/100 * 2)+txtExtra;
	
	lifeDescTxt.x=canvasW/100 * 88;
	lifeDescTxt.y=(canvasH/100 * 4)+txtExtra;
	lifeTxt.x=canvasW/100 * 95;
	lifeTxt.y=(canvasH/100 * 2)+txtExtra;
	
	scoreDisplayTxt = new createjs.Text();
	scoreDisplayTxt.font = "150px comfortaabold";
	scoreDisplayTxt.color = "#ffffff";
	scoreDisplayTxt.alpha=0;
	scoreDisplayTxt.textAlign = "center";
	
	
	gameContainer.addChild(bgGame, gamePlayer, gameBallShadow, gameBall, powerBarBg, powerBarBlank, powerBar, scoreDescTxt, scoreTxt, lifeDescTxt, lifeTxt, scoreDisplayTxt);
	
	bgOverlay = new createjs.Shape();
	bgOverlay.graphics.beginFill("#000").drawRect(0, 0, canvasW, canvasH);
	bgOverlay.alpha=.5;
	bgPop = new createjs.Bitmap(loader.getResult("bgPop"));
	
	centerReg(bgPop);
	bgPop.x=canvasW/2;
	bgPop.y=canvasH/2;
	
	btnReplay = new createjs.Bitmap(loader.getResult("btnReplay"));
	btnBackMain = new createjs.Bitmap(loader.getResult("btnBackMain"));
	btnFb = new createjs.Bitmap(loader.getResult("btnFb"));
	btnTwitter = new createjs.Bitmap(loader.getResult("btnTwitter"));
	btnGoogle = new createjs.Bitmap(loader.getResult("btnGoogle"));
	
	centerReg(btnReplay);
	createHitarea(btnReplay);
	centerReg(btnBackMain);
	createHitarea(btnBackMain);
	
	centerReg(btnFb);
	createHitarea(btnFb);
	
	centerReg(btnTwitter);
	createHitarea(btnTwitter);
	
	centerReg(btnGoogle);
	createHitarea(btnGoogle);
	
	btnReplay.x=canvasW/100 * 38;
	btnBackMain.x=canvasW/100 * 62;
	btnFb.x=canvasW/100*40;
	btnTwitter.x=canvasW/2;
	btnGoogle.x=canvasW/100*60;
	
	btnReplay.y=btnBackMain.y=canvasH/100*75;
	btnFb.y=btnTwitter.y=btnGoogle.y=canvasH/100*60;
	
	resultScoreDescTxt = new createjs.Text();
	resultScoreDescTxt.font = "50px comfortaabold";
	resultScoreDescTxt.color = "#ffffff";
	resultScoreDescTxt.text = gameOverText;
	resultScoreDescTxt.textAlign = "center";
	
	resultScoreTxt = new createjs.Text();
	resultScoreTxt.font = "150px comfortaabold";
	resultScoreTxt.color = "#ffffff";
	resultScoreTxt.text = '0';
	resultScoreTxt.textAlign = "center";
	
	resultShareTxt = new createjs.Text();
	resultShareTxt.font = "30px comfortaabold";
	resultShareTxt.color = "#ffffff";
	resultShareTxt.text = shareText;
	resultShareTxt.textAlign = "center";
	
	resultScoreDescTxt.x=resultScoreTxt.x=resultShareTxt.x=canvasW/2;
	resultScoreDescTxt.y=(canvasH/100*20)+txtExtra;
	resultScoreTxt.y=(canvasH/100*30)+txtExtra;
	resultShareTxt.y=(canvasH/100*50)+txtExtra;
	
	resultPopContainer.addChild(bgOverlay, bgPop, btnReplay, btnBackMain, btnFb, btnTwitter, btnGoogle, resultScoreDescTxt, resultScoreTxt, resultShareTxt);
	canvasContainer.addChild(mainContainer, gameContainer, resultPopContainer);
	stage.addChild(canvasContainer)
}


/*!
 * 
 * RESIZE GAME CANVAS - This is the function that runs to resize game canvas
 * 
 */
function resizeCanvas(){
 	if(canvasContainer!=undefined){
		canvasContainer.scaleX=canvasContainer.scaleY=scalePercent;
	}
}

/*!
 * 
 * REMOVE GAME CANVAS - This is the function that runs to remove game canvas
 * 
 */
 function removeGameCanvas(){
	 stage.autoClear = true;
	 stage.removeAllChildren();
	 stage.update();
	 createjs.Ticker.removeEventListener("tick", tick);
	 createjs.Ticker.removeEventListener("tick", stage);
 }

/*!
 * 
 * CANVAS LOOP - This is the function that runs for canvas loop
 * 
 */ 
function tick(event) {
	updateGame();
	stage.update(event);
}

/*!
 * 
 * CANVAS MISC FUNCTIONS
 * 
 */
function centerReg(obj){
	obj.regX=obj.image.naturalWidth/2;
	obj.regY=obj.image.naturalHeight/2;
}

function createHitarea(obj){
	obj.hitArea = new createjs.Shape(new createjs.Graphics().beginFill("#000").drawRect(0, 0, obj.image.naturalWidth, obj.image.naturalHeight));	
}