////////////////////////////////////////////////////////////
// BOX2D
////////////////////////////////////////////////////////////
var boxW=0;
var boxH=0;
var boxScale=30;
var box2dCanvasHolder="box2dCanvas";
var box2dPause=true;

var   b2Vec2
	,  b2AABB
	,	b2BodyDef
	,	b2Body
	,	b2FixtureDef
	,	b2Fixture
	,	b2World
	,	b2MassData
	,	b2PolygonShape
	,	b2CircleShape
	,	b2DebugDraw
	,  b2MouseJointDef
	;
 
var world
var fixDef
var bodyDef
var box2dInterval
var b2Listener

var worldGravity=50;

/*!
 * 
 * START BOX2D - This is the function that runs to setup box2d
 * 
 */
function initBox2D(w,h){
	boxW=w;
	boxH=h;
	
	b2Vec2 = Box2D.Common.Math.b2Vec2
	b2AABB = Box2D.Collision.b2AABB
	b2BodyDef = Box2D.Dynamics.b2BodyDef
	b2Body = Box2D.Dynamics.b2Body
	b2FixtureDef = Box2D.Dynamics.b2FixtureDef
	b2Fixture = Box2D.Dynamics.b2Fixture
	b2World = Box2D.Dynamics.b2World
	b2MassData = Box2D.Collision.Shapes.b2MassData
	b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape
	b2CircleShape = Box2D.Collision.Shapes.b2CircleShape
	b2DebugDraw = Box2D.Dynamics.b2DebugDraw
	b2MouseJointDef =  Box2D.Dynamics.Joints.b2MouseJointDef
 
	world = new b2World(
		   new b2Vec2(0, 50)    //gravity
		,  true                 //allow sleep
	 );
	 
	fixDef = new b2FixtureDef;
	fixDef.density = 1.0;
	fixDef.friction = 1.0;
	fixDef.radius = 20;
	fixDef.restitution = 1;
	
	bodyDef = new b2BodyDef;
	
	b2Listener = Box2D.Dynamics.b2ContactListener;
	setCollision()
	
	world.SetContactListener(b2Listener);
	updateWorldSetting()
}

/*!
 * 
 * BOX2D GRAVITY - This is the function that runs for box2d gravity
 * 
 */
function updateWorldSetting(){
	var gravity = new b2Vec2( 0, worldGravity);
	world.SetGravity(gravity);
}

/*!
 * 
 * BUILD BOX2D WORLD - This is the function that runs to setup box2d ground
 * 
 */
function createBox2DWorld() {
	var edge=5;
	
	//create ground
	bodyDef.type = b2Body.b2_staticBody;
	fixDef.shape = new b2PolygonShape;
	fixDef.shape.SetAsBox(boxW/boxScale, 5/boxScale);
	bodyDef.position.Set(0, (boxH/100*76)/boxScale);
	bodyDef.userData = 'bottom'
	world.CreateBody(bodyDef).CreateFixture(fixDef);
	 
	//setup debug draw
	var debugDraw = new b2DebugDraw();
	debugDraw.SetSprite(document.getElementById(box2dCanvasHolder).getContext("2d"));
	debugDraw.SetDrawScale(30.0);
	debugDraw.SetFillAlpha(.5);
	debugDraw.SetLineThickness(1.0);
	debugDraw.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit);
	world.SetDebugDraw(debugDraw);
	box2dInterval = setInterval(updateBox2d, 1000 / 60);
};

/*!
 * 
 * REMOVE BOX2D - This is the function that runs to remove box2d environment
 * 
 */
function removeBox2D(){
	clearInterval(box2dInterval);
	removeAllBody();
}


/*!
 * 
 * BOX2D COLLISION - This is the function that runs for box2d collision
 * 
 */
 function setCollision(){
	 	b2Listener.BeginContact = function(contact) {
						
		}
		b2Listener.EndContact = function(contact) {
			for(n=0;n<binplace_arr.length;n++){
				if (contact.GetFixtureA().GetBody().GetUserData()==('bin'+n+'_left')&&contact.GetFixtureB().GetBody().GetUserData()=='soccerBall') {
					playSound('soundBallHit',false);
				}
				if (contact.GetFixtureA().GetBody().GetUserData()==('bin'+n+'_right')&&contact.GetFixtureB().GetBody().GetUserData()=='soccerBall') {
					playSound('soundBallHit',false);
				}
				if (contact.GetFixtureA().GetBody().GetUserData()==('bin'+n+'_hit')&&contact.GetFixtureB().GetBody().GetUserData()=='soccerBall') {
					playSound('soundScore',false);
					updateScore(n+1);
				}
			}
			if (contact.GetFixtureA().GetBody().GetUserData()==('bottom')&&contact.GetFixtureB().GetBody().GetUserData()=='soccerBall') {
				playSound('soundBallHit',false);
				hitGround();
			}
		}
		b2Listener.PostSolve = function(contact, impulse) {
			
		}
		b2Listener.PreSolve = function(contact, oldManifold) {
			
		}
 }

/*!
 * 
 * BOX2D LOOP - This is the function that runs for box2d loop
 * 
 */
function updateBox2d() {	
	if(box2dPause){
		world.Step(1 / 60, 10, 10);
		//world.DrawDebugData();
		world.ClearForces();
	}
};

/*!
 * 
 * BOX2D BODY RETRIEVE - This is the function that runs to get box2d bodies by data
 * 
 */
function getBox2dData(data){
	for (b = world.GetBodyList(); b; b = b.GetNext()) {
		if(b.GetUserData()==data){
			return b;
		}
	}
}

/*!
 * 
 * BOX2D BALL UPDATE - This is the function that runs to update box2d ball position
 * 
 */
function updateBox2dBallPos(posX, posY){
	var target=getBox2dData('soccerBall');
	if(target!=null){
		target.SetPosition (new b2Vec2(posX/boxScale,posY/boxScale));
	}
}

/*!
 * 
 * CREATE BOX2D BUCKET - This is the function that runs to create box2d bin
 * 
 */
function drawBox2dBin(w,h,x,y,name){
	var target
	target=getBox2dData(name+'_hit');
	if(target!=null){
		world.DestroyBody(target);
	}
	
	bodyDef.type = b2Body.b2_staticBody;
	fixDef.shape = new b2PolygonShape;
	fixDef.shape.SetAsBox((w/3)/boxScale,1/boxScale);
	bodyDef.position.Set(x/boxScale, (y/boxScale)-((h/2)/boxScale));
	bodyDef.userData=name+'_hit';
	world.CreateBody(bodyDef).CreateFixture(fixDef);
	
	target=getBox2dData(name+'_right');
	if(target!=null){
		world.DestroyBody(target);
	}
	
	bodyDef.type = b2Body.b2_staticBody;
	fixDef.shape = new b2PolygonShape;
	fixDef.shape.SetAsBox(1/boxScale,(h/2)/boxScale);
	bodyDef.position.Set((x/boxScale)+((w/2)/boxScale), (y/boxScale)-((h/2)/boxScale));
	bodyDef.userData=name+'_right';
	world.CreateBody(bodyDef).CreateFixture(fixDef);
	
	target=getBox2dData(name+'_left');
	if(target!=null){
		world.DestroyBody(target);
	}
	
	bodyDef.type = b2Body.b2_staticBody;
	fixDef.shape = new b2PolygonShape;
	fixDef.shape.SetAsBox(1/boxScale,(h/2)/boxScale);
	bodyDef.position.Set((x/boxScale)-((w/2)/boxScale), (y/boxScale)-((h/2)/boxScale));
	bodyDef.userData=name+'_left';
	world.CreateBody(bodyDef).CreateFixture(fixDef);
}

/*!
 * 
 * CREATE BOX2D BALL - This is the function that runs to create box2d ball
 * 
 */
function drawBox2dBall(x,y,power){
	var target=getBox2dData('soccerBall');
	var targetX=x/boxScale;
	var targetY=y/boxScale;
	var velX=0;
	var velY=0;
	if(target!=null){
		world.DestroyBody(target);
	}
	var ballWidth=.6;
	bodyDef.type = b2Body.b2_dynamicBody;
	fixDef.shape = new b2CircleShape(ballWidth);
	bodyDef.position.x = targetX;
	bodyDef.position.y = targetY;
	bodyDef.userData = 'soccerBall';
	world.CreateBody(bodyDef).CreateFixture(fixDef);
	
	var targetBall=getBox2dData('soccerBall');
	var newVelocity = createHitVelocity(power);
	targetBall.SetLinearVelocity(newVelocity);
}

/*!
 * 
 * BOX2D VELOCITY - This is the function that runs to get bodies to move at constant speed
 * 
 */
function createHitVelocity(power){
	power = power < 10 ? 10 : power;
	
	var farNum = 28;
	var velX=(power/100*farNum);
	
	if(power >= 0 && power <30){
		velY = -35;
	}else if(power >= 31 && power <50){
		velY = -30;
	}else{
		velY = -28;
	}
	
	return new b2Vec2(velX, velY);
}

/*!
 * 
 * REMOVE BOX2D BODY - This is the function that runs to remove bo2d body
 * 
 */
function removeBody(targetname){
	var target=getBox2dData(targetname);
	if(target!=null){
		world.DestroyBody(target);
	}
}

/*!
 * 
 * REMOVE ALL BOX2D BODIES - This is the function that runs to remove all bo2d bodies
 * 
 */
function removeAllBody(){
	for (b = world.GetBodyList(); b; b = b.GetNext()) {
		world.DestroyBody(b)
	}
}

/*!
 * 
 * TOGGLE BOX2D WORLD - This is the function that runs to start or stop box2d loop
 * 
 */
function toggleBox2dWorld(con){
	box2dPause=con
}