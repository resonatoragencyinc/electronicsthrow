////////////////////////////////////////////////////////////
// GAME
////////////////////////////////////////////////////////////

/*!
 * 
 * GAME SETTING CUSTOMIZATION START
 * 
 */
var scoreDisplayText = 'SCORE'; //text for gameplay score
var lifeDisplayText = 'LIFE'; //text for gameplay life
var gameOverText = 'FINAL SCORE'; //text for gameplay result
var shareText ='SHARE ON'; //text for share instruction
var playerLife = 3; //total life

//list of bins
var binobject_arr=[{src:'assets/binGreen.png', score:5},
				{src:'assets/binRed.png', score:10},
				{src:'assets/binGrey.png', score:15}];

//number of bins and position place
var binplace_arr=[45, 60, 75];
				
//Social share, [SCORE] will replace with game score
var shareTitle = 'Highscore on Trick Shot Ball is [SCORE]';//social share score title
var shareMessage = '[SCORE] is mine new highscore on Trick Shot Ball! Try it now!'; //social share score message

/*!
 *
 * GAME SETTING CUSTOMIZATION END
 *
 */
 
 
 $.binHolder = {};
 var gameEnd=false;
 var gameReadyReset=false;
 var playerAnime='';
 var gamePause=true;
 var playerData=[{
				playerScore:0,
 				playerLife:0}];

/*!
 * 
 * GAME BUTTONS - This is the function that runs to setup button event
 * 
 */
function buildGameButton(){
	//main
	btnStart.cursor = "pointer";
	btnStart.addEventListener("mousedown", function(evt) {
		playSound('soundClick',false);
		goPage('game');
		createGame();
		startGame();
	});
	
	btnReplay.cursor = "pointer";
	btnReplay.addEventListener("mousedown", function(evt) {
		playSound('soundClick',false);
		goPage('game');
		createGame();
		startGame();
	});
	
	btnBackMain.cursor = "pointer";
	btnBackMain.addEventListener("mousedown", function(evt) {
		playSound('soundClick',false);
		goPage('main');
	});
	
	btnFb.cursor = "pointer";
	btnFb.addEventListener("click", function(evt) {
		share('facebook');
	});
	btnTwitter.cursor = "pointer";
	btnTwitter.addEventListener("click", function(evt) {
		share('twitter');
	});
	btnGoogle.cursor = "pointer";
	btnGoogle.addEventListener("click", function(evt) {
		share('google');
	});
}

function setupGameButton(){
	stage.cursor = "pointer";
	stage.addEventListener("click", handlerMethod);
}

function removeGameButton(){
	stage.cursor = null;
	stage.removeEventListener("click", handlerMethod);
}

function handlerMethod(evt) {
	 switch (evt.type){
		 case 'click':
		 	if(!gamePause)
		 		touchPower();
		 	break;
	 }
}

/*!
 * 
 * DISPLAY PAGES - This is the function that runs to display pages
 * 
 */
function goPage(page){
	gameContainer.visible=false;
	mainContainer.visible=false;
	resultPopContainer.visible=false;	
	bgOverlay.visible=bgPop.visible=false;
	
	switch(page){
		case 'main':
			mainContainer.visible=true;
		break;
		case 'game':
			gameContainer.visible=true;
		break;
	}
}


/*!
 * 
 * CREATE GAME - This is the function that runs to create new game
 * 
 */
 function createGame(){
	gameBall.visible=true;
	playerData[0].playerScore=0; 
	playerData[0].playerLife=playerLife;
	updateLife();
	
	updateScore(0);
	resetGame();
 }
 
 /*!
 * 
 * START GAME - This is the function that runs to start play game
 * 
 */
 function startGame(){
	 setupGameButton();
	 toggleBox2dWorld(true);
	 setTimeout(function() {
		 gamePause=false;
	 }, 100);
 }
 
 /*!
 * 
 * STOP GAME - This is the function that runs to stop play game
 * 
 */
 function stopGame(){
	  removeGameButton();
	  
	  gamePause=true;
	  toggleBox2dWorld(false);
	  gameBall.visible=false;
	  
	  showResult();
 }
 
 /*!
 * 
 * DISPLAY GAME RESULT - This is the function that runs to display game result
 * 
 */
 function showResult(){
	 playSound('soundFail',false);
	 
	 bgOverlay.visible=bgPop.visible=true;
	 resultPopContainer.visible=true;
 }

 /*!
 * 
 * RESET GAME - This is the function that runs to reset game
 * 
 */
function resetGame(){
	gameReadyReset=gameEnd=false;
	
	removeBody('soccerBall');
	animatePlayer('static');
	
	gameBall.visible=true;
	gameBall.x=canvasW/100*22;
	gameBall.y=canvasH/100*72;
	gameBallShadow.x=gameBall.x
	gameBallShadow.y=canvasH/100*75;
	gameBallShadow.scaleX=gameBallShadow.scaleY=1;
	
	touchPowerStat=0;
	powerValue.percent=0;
	drawPower();
	
	createBin();
}
 
 /*!
 * 
 * GAME LOOP - This is the function that runs for game loop
 * 
 */
 function updateGame(){
	if(!gamePause){
		var _soccerBall=getBox2dData('soccerBall')
		if(_soccerBall!=null){
			gameBall.x=_soccerBall.GetPosition().x*boxScale;
			gameBall.y=_soccerBall.GetPosition().y*boxScale;
			gameBall.rotation=_soccerBall.GetPosition().x*boxScale;
			gameBallShadow.x=gameBall.x;
			
			var scale = gameBall.y/canvasW*20;
			scale = scale * .1;
			scale = scale > 1 ? 1 : scale;
			scale = scale < .3 ? .3 : scale;
			gameBallShadow.scaleX=gameBallShadow.scaleY=scale;
		}
		checkBallCollision();
		checkPlayerEndFrame();
	} 
 }

 /*!
 * 
 * POWER BAR EVENT - This is the function that runs for power bar trigger event
 * 
 */ 
 var touchPowerStat = 0;
 function touchPower(){
	 if(touchPowerStat==0){
		 //trigger power
		touchPowerStat = 1;
		startAnimatePower(); 
	 }else if(touchPowerStat==1){
		 //final power
		touchPowerStat = 2;
		stopAnimatePower();
		if(powerValue.percent>40){
			animatePlayer('hitpower');
		}else{
			animatePlayer('hitsoft');	
		}
	 }
 }
 
 /*!
 * 
 * START POWER BAR ANIMATION - This is the function that runs for start power bar animation
 * 
 */ 
 var powerValue = { percent: 0 };
 function startAnimatePower(){
	 stopAnimatePower();
	
	var newPercent = powerValue.percent;
	if(newPercent==0){
		newPercent=100;
	}else{
		newPercent=0;
	}
	
	$(powerValue).clearQueue().stop().animate({
      percent: newPercent
    }, {duration: 500,
			step: function(){
				  drawPower();
		},complete: function() {
			startAnimatePower();
		}
	});
 }
 
 /*!
 * 
 * DRAW POWER BAR - This is the function that runs for power bar mask draw
 * 
 */
 function drawPower(){
	 var startX = powerBar.x - (powerBar.image.naturalWidth/2);
	 var startY = powerBar.y - (powerBar.image.naturalHeight/2);
	 var barWidth = 619;
	 var barHeight = 46;
	 
	 powerBarMaskBitmap.graphics.clear();
	 powerBarMaskBitmap.graphics.beginFill("black");
	
	 var percent = (powerValue.percent/100)*barWidth;
	 powerBarMaskBitmap.graphics.moveTo(startX, startY).lineTo(startX+percent, startY).lineTo(startX+(percent-24), startY+barHeight).lineTo(startX, startY+barHeight).lineTo(startX, startY);
 }
 
 
 /*!
 * 
 * STOP POWER BAR ANIMATION - This is the function that runs for stop power animation
 * 
 */
 function stopAnimatePower(){
	$(powerValue).clearQueue().stop(true, false);
 }

 /*!
 * 
 * CHECK PLAYER ANIMATION FRAME - This is the function that runs to check if player animation is complete
 * 
 */ 
 function checkPlayerEndFrame(){
	 if(touchPowerStat==2){
		 if(gamePlayer.currentFrame==3 || gamePlayer.currentFrame==6){
			 playSound('soundBallHit',false);
			drawBox2dBall(gameBall.x, gameBall.y, powerValue.percent);
			touchPowerStat=3;
		 }
	 }
 }

/*!
 * 
 * PLAYER ANIMATION - This is the function that runs to play player animation
 * 
 */ 
function animatePlayer(con){
	if(playerAnime!=con){
		playerAnime=con
		gamePlayer.gotoAndPlay(con)
	}
}


/*!
 * 
 * GENERATE BIN - This is the function that runs to generate bin and posiiton
 * 
 */
function generateBinPlace(){
	var temp_arr = [];
	for(n=0;n<binplace_arr.length;n++){
		temp_arr.push({id:0, pos:binplace_arr[n]});
	}	
	binplace_arr = temp_arr;
}
generateBinPlace();

/*!
 * 
 * CREATE BIN - This is the function that runs to create bins
 * 
 */
function createBin(){
	getRandomBin();
	
	for(n=0;n<binplace_arr.length;n++){
		gameContainer.removeChild($.binHolder['bin'+n]);
		$.binHolder['bin'+n] = $.bin['bin' + binplace_arr[n].id].clone();
		$.binHolder['bin'+n].x = Math.round((canvasW/100*binplace_arr[n].pos)+getBinRandomPosX());
	}
	
	for(n=1;n<binplace_arr.length;n++){
		var rangeX = $.binHolder['bin'+n].x - $.binHolder['bin'+(n-1)].x;
		var rangeW = ($.binHolder['bin'+n].image.naturalWidth/2) + ($.binHolder['bin'+(n-1)].image.naturalWidth/2);
		
		if(rangeX<rangeW){
			$.binHolder['bin'+n].x=$.binHolder['bin'+(n-1)].x+rangeW+10;
		}
	}
	
	for(n=0;n<binplace_arr.length;n++){
		gameContainer.addChild($.binHolder['bin'+n]);
	}
	positionBin();
}


/*!
 * 
 * POSITION BIN - This is the function that runs to set bins position
 * 
 */
function positionBin(){
	for(n=0;n<binplace_arr.length;n++){
		$.binHolder['bin'+n].y = canvasH/100*75;
		
		var binH;
		drawBox2dBin($.binHolder['bin'+n].image.naturalWidth, $.binHolder['bin'+n].image.naturalHeight, $.binHolder['bin'+n].x, $.binHolder['bin'+n].y, 'bin'+n);	
	}
}

/*!
 * 
 * GET RANDOM BIN - This is the function that runs to get random Bin type
 * 
 */
function getRandomBin(){
	for(n=0;n<binplace_arr.length;n++){
		binplace_arr[n].id = Math.round(Math.random()*(binobject_arr.length-1));
	}
}


/*!
 * 
 * GET BIN RANDOM POSITION X - This is the function that runs to get Bin random position x
 * 
 */
function getBinRandomPosX(){
	return Math.round(Math.random()*130);
}


/*!
 * 
 * CHECK BALL COLLISION - This is the function that runs to check if ball is out of screen
 * 
 */
function checkBallCollision(){
	if(gameBall.x>canvasW+50){
		offScreen();
	}else if(gameBall.x<-50){
		offScreen();
	}
}


/*!
 * 
 * HIT GROUND - This is the function that runs when ball hit the ground
 * 
 */
function hitGround(){
	if(!gameEnd){
		gameEnd=true;
		setTimeout(function() {
			collisionEnd();
		}, 500);
	}
}

/*!
 * 
 * OFF SCREEN - This is the function that runs when ball is out of screen
 * 
 */
function offScreen(){
	if(!gameEnd){
		gameEnd=true;
		collisionEnd();
	}
}

/*!
 * 
 * COLLISION END - This is the function that runs when ball is hit the ground or off screen
 * 
 */
 function collisionEnd(){
	if(!gameReadyReset){
		gameReadyReset=true;
		$(gameBall).clearQueue().stop();
		decreaseLife();
		resetGame();
	}
}

/*!
 * 
 * UPDATE SCORE - This is the function that runs to add score
 * 
 */
function updateScore(num){
	if(num!=0 && !gameEnd){
		var score = binobject_arr[binplace_arr[num-1].id].score;
		playerData[0].playerScore+=score;
		scoreTxt.text = resultScoreTxt.text = playerData[0].playerScore;
		
		var startX1=canvasH/100*30;
		scoreDisplayTxt.text = '+'+score+'PT';
		scoreDisplayTxt.x=canvasW/2;
		scoreDisplayTxt.y=startX1;
		scoreDisplayTxt.alpha=0;
		
		$(scoreDisplayTxt)
		.clearQueue()
		.stop()
		.animate({ alpha:1, y:startX1-(canvasH/100*6)}, 500 )
		.animate({ alpha:0}, 500 );
	}else{
		scoreTxt.text = 0;	
	}
	
	setTimeout(function() {
		 resetGame();
	}, 50);
}

/*!
 * 
 * DECREASE LIFE - This is the function that runs to decrease life
 * 
 */
function decreaseLife(){
	playSound('soundLose',false);
	playerData[0].playerLife--;
	updateLife();
	if(playerData[0].playerLife<1){
		stopGame();	
	}
}

/*!
 * 
 * UPDATE LIFE - This is the function that runs to display current life
 * 
 */
function updateLife(){
	lifeTxt.text = playerData[0].playerLife;
}


/*!
 * 
 * SHARE - This is the function that runs to open share url
 * 
 */
function share(action){
	var loc = location.href
	loc = loc.substring(0, loc.lastIndexOf("/") + 1);
	var title = shareTitle.replace("[SCORE]", playerData[0].playerScore);
	var text = shareMessage.replace("[SCORE]", playerData[0].playerScore);
	var shareurl = '';
	
	if( action == 'twitter' ) {
		shareurl = 'https://twitter.com/intent/tweet?url='+loc+'&text='+text;
	}else if( action == 'facebook' ){
		shareurl = 'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(loc+'share.php?desc='+text+'&title='+title+'&url='+loc+'&thumb='+loc+'share.jpg&width=590&height=300');
	}else if( action == 'google' ){
		shareurl = 'https://plus.google.com/share?url='+loc;
	}
	
	window.open(shareurl);
}